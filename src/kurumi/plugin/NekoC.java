package kurumi.plugin;

import org.bukkit.plugin.java.*;
import org.bukkit.configuration.file.*;
import java.util.*;
import org.bukkit.command.*;
import org.bukkit.*;
import org.bukkit.event.*;
import org.bukkit.plugin.*;
import org.bukkit.entity.*;
import kurumi.plugin.events.*;
import kurumi.plugin.commands.*;
public class NekoC extends JavaPlugin
{
    public FileConfiguration config;
    public static List<String> nekoListP;
    public static List<String> kittenListP;
    public static boolean globalCommands;

    public NekoC() {
        this.config = this.getConfig();
    }

    public void onEnable() {
        this.saveDefaultConfig();
        NekoC.globalCommands = this.config.getBoolean("GlobalCommandMessages");
        NekoC.nekoListP = (List<String>)this.config.getStringList("Nekos");
        NekoC.kittenListP = (List<String>)this.config.getStringList("Kittens");
        MeatOnly.registerUnedibleItems();
        if (this.config.getBoolean("Pet")) {
            this.getCommand("Pet").setExecutor((CommandExecutor)new Pet());
        }
        if (this.config.getBoolean("Lovebite")) {
            this.getCommand("Lovebite").setExecutor((CommandExecutor)new lovebite());
        }
        if (this.config.getBoolean("Nightvision")) {
            this.getCommand("nightvision").setExecutor((CommandExecutor)new Nightvision());
        }
        if (this.config.getBoolean("Purr")) {
            this.getCommand("Purr").setExecutor((CommandExecutor)new Purr());
        }
        if (this.config.getBoolean("EarScratch")) {
            this.getCommand("EarScratch").setExecutor((CommandExecutor)new EarScratch());
        }
        if (this.config.getBoolean("Attention")) {
            this.getCommand("attention").setExecutor((CommandExecutor)new Attention());
        }
        if (this.config.getBoolean("Hiss")) {
            this.getCommand("hiss").setExecutor((CommandExecutor)new Hiss());
        }
        if (this.config.getBoolean("Scratch")) {
            this.getCommand("scratch").setExecutor((CommandExecutor)new Scratch());
        }
        if (this.config.getBoolean("NekoChat")) {
            Bukkit.getPluginManager().registerEvents((Listener)new NekoChat(), (Plugin)this);
        }
        if (this.config.getBoolean("MeatOnly")) {
            Bukkit.getPluginManager().registerEvents((Listener)new MeatOnly(), (Plugin)this);
        }
        this.getCommand("nekotf").setExecutor((CommandExecutor)new NekoTF());
    }

    public void onDisable() {
    }

    public static boolean isNeko(final Player p) {
        final String name = p.getName();
        return NekoC.nekoListP.contains(name) || isKitten(p);
    }

    public static boolean isKitten(final Player p) {
        final String name = p.getName();
        return NekoC.kittenListP.contains(name);
    }
}
