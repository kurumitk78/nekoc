package kurumi.plugin.kittens;

import org.bukkit.event.player.*;
import org.bukkit.event.block.*;
import kurumi.plugin.*;
import org.bukkit.*;
import org.bukkit.event.*;

public class AntiDoorOpen implements Listener
{
    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent event) {
        if (event.getAction().equals((Object)Action.RIGHT_CLICK_BLOCK) && (event.getClickedBlock().getType() != null || event.getClickedBlock().getType() != Material.AIR) && NekoC.isKitten(event.getPlayer()) && (event.getClickedBlock().getType() == Material.DARK_OAK_DOOR || event.getClickedBlock().getType() == Material.DARK_OAK_FENCE_GATE || event.getClickedBlock().getType() == Material.DARK_OAK_TRAPDOOR)) {
            event.getPlayer().sendMessage(ChatColor.GREEN + "[NekoC]" + ChatColor.LIGHT_PURPLE + " You cannot open that door");
            event.setCancelled(true);
        }
    }
}
